﻿using System.Collections.Generic;

namespace Code.Models
{
    [System.Serializable]
    public class CalculatorSaveData
    {
        public string LastInputString;

        public List<string> ProccesedLines;

        public CalculatorSaveData() 
        { 
            ProccesedLines = new List<string>();
        }
    }
}
