using System;

namespace Code.Models
{
    public class Calculator
    {
        public string GetResult(string inputString)
        {
            string[] results = inputString.Split('+');

            if(CanAddition(inputString, results))
            {
                string result = Addition(Convert.ToInt32(results[0]), Convert.ToInt32(results[1]));
                return $"{inputString}={result}";
            }

            return ReturnError(inputString);
        }

        private bool CanAddition(string inputString, string[] results)
        {
            if (inputString.Contains(" "))
                return false;

            if(results.Length < 2 || results.Length > 2) return false;

            if (!int.TryParse(results[0], out var _) || !int.TryParse(results[1], out var _))
            {
                return false;
            }

            return true;
        }

        private string Addition(int a, int b)
        {
            return (a + b).ToString();
        }

        private string ReturnError(string inputString) => $"{inputString}=ERROR";
    }
}
