using Code.Data;

namespace Code.Services.PersistentProgress
{
    public interface IPersistentProgressService
    {
        GameProgress Progress { get; set; }
    }
}