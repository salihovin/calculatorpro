using Code.Data;

namespace Code.Services.PersistentProgress
{
    public class PersistentProgressService : IPersistentProgressService
    {
        public GameProgress Progress { get; set; }
    }
}