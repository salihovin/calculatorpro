﻿using Code.Models;
using Code.Presenters;
using Code.Services.PersistentProgress;
using Code.Services.SaveLoad;
using Code.Views;
using UnityEngine;

namespace Code.Infrastructure
{
    public class Bootstraper : MonoBehaviour
    {
        [SerializeField]
        private CalculatorView _calculatorView;

        private CalculatorPresenter _calculatorPresenter;
        private SaveLoadPresenter _saveLoadPresenter;

        private void Awake()
        {
            RegisterServices();
        }

        private void OnEnable()
        {
            _calculatorPresenter.Enable();
            _saveLoadPresenter.Enable();
        }

        private void Start()
        {
            _saveLoadPresenter.Load();
        }

        private void RegisterServices()
        {
            Calculator calculator = new Calculator();

            IPersistentProgressService progressService = new PersistentProgressService();
            ISaveLoadService saveLoad = new SaveLoadService(progressService);

            _calculatorPresenter = new CalculatorPresenter(_calculatorView, calculator);
            _saveLoadPresenter = new SaveLoadPresenter(saveLoad, progressService, _calculatorView);
        }
    }
}