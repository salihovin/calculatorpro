using Sirenix.OdinInspector;
using System;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Code.Views
{
    public class CalculatorView : MonoBehaviour
    {
        [SerializeField]
        private RectTransform _parentTransform;

        [SerializeField]
        private TMP_InputField _inputField;

        [SerializeField]
        private Button _button;

        [SerializeField]
        private TextMeshProUGUI _uIStringPrefab;

        private List<TextMeshProUGUI> _uiStings = new();

        public void SubcribeButton(Action action)
        {
            _button.onClick.AddListener(() => action?.Invoke());
        }

        public void SubscribeInputField(Action<string> action)
        {
            _inputField.onValueChanged.AddListener((text) => action.Invoke(text));
        }

        public string ReadInputStringAndClear()
        {
            string inputString = _inputField.text;
            _inputField.text = string.Empty;
            return inputString;
        }

        public string ReadInputString()
        {
            return _inputField.text;
        }

        public List<string> GetListStrings()
        {
            return _uiStings.Select(x => x.text).ToList();
        }

        public void SetInputString(string inputString)
        {
            _inputField.text = inputString;
        }

        [Button]
        public void AddStringToQuery(string inputString)
        {
            InstatiateUIString(inputString);
        }

        public void AddStringList(List<string> strings)
        {
            for (int i = 0; i < strings.Count; i++)
            {
                AddStringToQuery(strings[i]);
            }
        }

        private void InstatiateUIString(string text)
        {
            var uiString = Instantiate(_uIStringPrefab, _parentTransform);
            uiString.transform.SetSiblingIndex(GetSiblingIndex());
            uiString.SetText(text);
            _uiStings.Add(uiString);
        }

        private int GetSiblingIndex()
        {
            if (_uiStings.Count > 0)
            {
                return _uiStings.Last().transform.GetSiblingIndex() + 1;
            }

            return _inputField.transform.GetSiblingIndex() + 1;
        }

        [Button]
        private void ClearUIstrings()
        {
            for (int i = 0; i < _uiStings.Count; i++)
            {
                var uistringGo = _uiStings[i];

                Destroy(uistringGo.gameObject);

                _uiStings.Remove(uistringGo);
            }

            _uiStings.Clear();
        }
    }
}
