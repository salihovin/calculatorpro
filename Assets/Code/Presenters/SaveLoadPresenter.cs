﻿using Code.Data;
using Code.Models;
using Code.Services.PersistentProgress;
using Code.Services.SaveLoad;
using Code.Views;

namespace Code.Presenters
{
    public class SaveLoadPresenter
    {
        private readonly ISaveLoadService _saveLoadService;
        private readonly IPersistentProgressService _progressService;
        private readonly CalculatorView _calculatorView;

        public SaveLoadPresenter(ISaveLoadService saveLoadService,
                                 IPersistentProgressService progressService,
                                 CalculatorView calculatorView)
        {
            _saveLoadService = saveLoadService;
            _progressService = progressService;
            _calculatorView = calculatorView;
        }

        public void Enable()
        {
            _calculatorView.SubscribeInputField(SaveInputLine);
            _calculatorView.SubcribeButton(SaveStringLines);
        }

        public void Load()
        {
            _progressService.Progress = _saveLoadService.LoadProgress();

            if (_progressService != null && _progressService.Progress != null)
            {
                _calculatorView.SetInputString(_progressService.Progress.CalculatorSaveData.LastInputString);
                _calculatorView.AddStringList(_progressService.Progress.CalculatorSaveData.ProccesedLines);
            }
        }

        private void SaveInputLine(string inputLine)
        {
            CreateProgress();
            _progressService.Progress.CalculatorSaveData.LastInputString = _calculatorView.ReadInputString();
            _saveLoadService.SaveProgress();
        }

        private void SaveStringLines()
        {
            CreateProgress();
            _progressService.Progress.CalculatorSaveData.ProccesedLines = _calculatorView.GetListStrings();
            _saveLoadService.SaveProgress();
        }

        private void CreateProgress()
        {
            if(_progressService.Progress == null)
            {
                _progressService.Progress = new GameProgress();
            }
        }
    }
}
