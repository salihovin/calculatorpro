using Code.Models;
using Code.Views;

namespace Code.Presenters
{
    public class CalculatorPresenter
    {
        private readonly CalculatorView _calculattorView;
        private readonly Calculator _calculator;

        public CalculatorPresenter(CalculatorView calculattorView, Calculator calculator)
        {
            _calculattorView = calculattorView;
            _calculator = calculator;
        }

        public void Enable()
        {
            _calculattorView.SubcribeButton(TryCalculate);
        }

        private void TryCalculate()
        {
            string inputString = _calculattorView.ReadInputStringAndClear();
            string result = _calculator.GetResult(inputString);
            _calculattorView.AddStringToQuery(result);
        }
    }
}
