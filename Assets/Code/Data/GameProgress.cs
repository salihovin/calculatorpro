using Code.Models;
using System;

namespace Code.Data
{
    [Serializable]
    public class GameProgress
    {
        public CalculatorSaveData CalculatorSaveData;

        public GameProgress() 
        {
            CalculatorSaveData = new CalculatorSaveData();
        }
    }
}